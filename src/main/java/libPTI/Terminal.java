package libPTI;

import Enums.PTISTAT;

public class Terminal {

    private PTISTAT status;
    private String id;
    private String model;
    private String macAdress;
    private String serial;

    public Terminal(String id){
        this.id = id;
    }

    public PTISTAT getStatus() {
        return status;
    }

    public void setStatus(PTISTAT status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMacAdress() {
        return macAdress;
    }

    public void setMacAdress(String macAdress) {
        this.macAdress = macAdress;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

}
