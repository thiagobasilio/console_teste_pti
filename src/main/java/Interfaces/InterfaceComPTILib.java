package Interfaces;
import com.sun.jna.Library;
import com.sun.jna.ptr.ShortByReference;

public interface InterfaceComPTILib extends Library {

    void PTI_Init( String pszPOS_Company, String pszPOS_Version, String pszPOS_Capabilities, String pszDataFolder,
                    short uiTCP_Port, short uiMaxTerminals, String pszWaitMsg, short uiAutoDiscSec,
                   ShortByReference piRet);

    void PTI_End();

    void PTI_ConnectionLoop ( byte[] pszTerminalId, byte[] pszModel, byte[] pszMAC, byte[] pszSerNo,
                              ShortByReference piRet);

    void PTI_CheckStatus ( byte[] pszTerminalId, ShortByReference piStatus, byte[] pszModel, byte[] pszMAC,
                           byte[] pszSerNo, ShortByReference piRet);

    void PTI_Disconnect (String pszTerminalId, short uiPwrDelay, ShortByReference piRet);

    void PTI_Display (String pszTerminalId, String pszMsg, ShortByReference piRet);

}
