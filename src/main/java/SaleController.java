import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import libPTI.Terminal;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;


public class SaleController implements Initializable {

    private String pos1, pos2, pos3;
    private int terminaisAtivos = 0;
    private CamadaPTI camadaPTI = new CamadaPTI();
    @FXML Pane pnTerm1;
    @FXML Pane pnTerm2;
    @FXML Pane pnTerm3;
    @FXML Label lblTerm1;
    @FXML Label lblTerm2;
    @FXML Label lblTerm3;
    @FXML Label lblStatus1;
    @FXML Label lblStatus2;
    @FXML Label lblStatus3;
    @FXML Label lblModelTerm1;
    @FXML Label lblModelTerm2;
    @FXML Label lblModelTerm3;
    @FXML Label lblMAC1;
    @FXML Label lblMAC2;
    @FXML Label lblMAC3;
    @FXML Label lblSerial1;
    @FXML Label lblSerial2;
    @FXML Label lblSerial3;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void liberarTerminais(int terminais, String pos1, String pos2, String pos3){
        this.pos1 = pos1;
        this.pos2 = pos2;
        this.pos3 = pos3;
        switch (terminais){
            case 0:
                pnTerm1.setDisable(false);
                lblTerm1.setText(pos1);
                atualizarTerminalLoop(pos1,pnTerm1);
                break;
            case 1:
                pnTerm1.setDisable(false); pnTerm2.setDisable(false);
                lblTerm1.setText(pos1); lblTerm2.setText(pos2);
                atualizarTerminalLoop(pos1,pnTerm1); atualizarTerminalLoop(pos2,pnTerm2);
                break;
            case 2:
                pnTerm1.setDisable(false); pnTerm2.setDisable(false); pnTerm3.setDisable(false);
                lblTerm1.setText(pos1); lblTerm2.setText(pos2); lblTerm3.setText(pos3);
                atualizarTerminalLoop(pos1,pnTerm1); atualizarTerminalLoop(pos2,pnTerm2);
                atualizarTerminalLoop(pos3,pnTerm3);
                break;
        }
        buscandoTerminais();
    }

    private boolean atualizarTerminal(String pos, Pane pnTerminal){
        Terminal terminal = camadaPTI.checkTerminal(pos);
        if(terminal != null) {
            Label lblTerm = (Label) pnTerminal.getChildren().get(1);
            Label lblModel = (Label) pnTerminal.getChildren().get(3);
            Label lblMAC = (Label) pnTerminal.getChildren().get(4);
            Label lblSerial = (Label) pnTerminal.getChildren().get(5);
            Label lblStatus = (Label) pnTerminal.getChildren().get(6);
            Button btnPagar = (Button) pnTerminal.getChildren().get(0);
            lblTerm.setText(terminal.getId());
            lblModel.setText(terminal.getModel());
            lblMAC.setText(terminal.getMacAdress());
            lblSerial.setText(terminal.getSerial());
            lblStatus.setText(terminal.getStatus().toString());

            btnPagar.setDisable(false);
            pnTerminal.setDisable(false);
            return true;
        }

        return false;
    }

    public void clickBtnPagarTerm1(ActionEvent actionEvent) {
        String pos = lblTerm1.getText();
        camadaPTI.display(pos,"AGUARDANDO O PROCESSO\rDE VENDA NA AUTOMACAO");
    }

    public void clickBtnPagarTerm2(ActionEvent actionEvent) { }

    public void clickBtnPagarTerm3(ActionEvent actionEvent) { }

    public void clickbtnDesconnect1(ActionEvent actionEvent) {
        String pos = lblTerm1.getText();
        if(camadaPTI.disconnect(pos,(short)0)){
            lblStatus1.setText("");
            lblTerm1.setText("");
            lblModelTerm1.setText("");
            lblMAC1.setText("");
            lblSerial1.setText("");
            pnTerm1.setDisable(true);
            atualizarTerminalLoop(pos,pnTerm1);
        }
    }

    public void clickbtnDesconnect2(ActionEvent actionEvent) {
        String pos = lblTerm2.getText();
        if(camadaPTI.disconnect(pos,(short)0)){
            lblStatus2.setText("");
            lblTerm2.setText("");
            lblModelTerm2.setText("");
            lblMAC2.setText("");
            lblSerial2.setText("");
            pnTerm2.setDisable(true);
            atualizarTerminalLoop(pos,pnTerm2);
        }
    }

    public void clickbtnDesconnect3(ActionEvent actionEvent) {
        String pos = lblTerm3.getText();
        if(camadaPTI.disconnect(pos,(short)0)){
            lblStatus3.setText("");
            lblTerm3.setText("");
            lblModelTerm3.setText("");
            lblMAC3.setText("");
            lblSerial3.setText("");
            pnTerm3.setDisable(true);
            atualizarTerminalLoop(pos,pnTerm3);
        }
    }

    private void buscandoTerminais(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    connectionLoop();
                });
            }
        }, 1000, 3000);
    }

    private void atualizarTerminalLoop(String pos, Pane pnTerm){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    if(atualizarTerminal(pos, pnTerm))
                        timer.cancel();

                    System.out.println("Buscando pos..." + pos);
                });
            }

        }, 1000, 3000);
    }


    public void connectionLoop(){
        Terminal terminal = camadaPTI.connectionLoop();
        if (terminal != null)
            createAlert(terminal);
    }

    private void createAlert(Terminal terminal){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("Terminal conectado!");
        alert.setContentText("POS: " + terminal.getId() + "\nModelo: " + terminal.getModel() +
                "\nMAC: " + terminal.getMacAdress() + "\nSerial: " + terminal.getSerial());
        alert.showAndWait();
    }

}

