import Enums.PTIRET;
import Interfaces.InterfaceComPTILib;
import com.sun.jna.Native;
import com.sun.jna.Platform;
import com.sun.jna.ptr.ShortByReference;

public class LibIntegrada {

    private InterfaceComPTILib pTI_Lib;
    private ShortByReference piRet;
    public LibIntegrada() {

        this.pTI_Lib = Native.loadLibrary(Platform.isLinux() ? "Nao disponivel" : "libPTI/PTI_DLL.dll",
                InterfaceComPTILib.class); //lendo a lib pela Interface
    }

    public PTIRET pTI_Init(String pszPOS_Company, String pszPOS_Version, String pszPOS_Capabilities, String pszDataFolder,
                           short uiTCP_Port, short uiMaxTerminals, String pszWaitMsg, short uiAutoDiscSec) {
        piRet = new ShortByReference();
        pTI_Lib.PTI_Init(pszPOS_Company, pszPOS_Version, pszPOS_Capabilities, pszDataFolder, uiTCP_Port, uiMaxTerminals,
                pszWaitMsg,uiAutoDiscSec,piRet);
        return convertPWRET(piRet);
    }

    public void pTI_End(){ pTI_Lib.PTI_End();}

    public PTIRET pTI_ConnectionLoop (byte[] pszTerminalId, byte[] pszModel, byte[] pszMAC, byte[] pszSerNo){
        piRet = new ShortByReference();
        pTI_Lib.PTI_ConnectionLoop(pszTerminalId, pszModel, pszMAC, pszSerNo,piRet);
        return convertPWRET(piRet);
    }

    public PTIRET pTI_CheckStatus(byte[] pszTerminalId, ShortByReference piStatus, byte[] pszModel, byte[] pszMAC,
                                  byte[] pszSerNo){
        piRet = new ShortByReference();
        pTI_Lib.PTI_CheckStatus(pszTerminalId,piStatus,pszModel,pszMAC, pszSerNo,piRet);
        return convertPWRET(piRet);
    }

    public PTIRET pTI_Disconnect (String pszTerminalId, short uiPwrDelay){
        piRet = new ShortByReference();
        pTI_Lib.PTI_Disconnect(pszTerminalId,uiPwrDelay,piRet);
        return convertPWRET(piRet);
    }

    public PTIRET pTI_Display (String pszTerminalId, String pszMsg){
        piRet = new ShortByReference();
        pTI_Lib.PTI_Display(pszTerminalId,pszMsg,piRet);
        return convertPWRET(piRet);
    }

    private PTIRET convertPWRET(ShortByReference value){
        for (PTIRET pwret : PTIRET.values()) {
            if (pwret.getValor() == value.getValue())
                return pwret;
        }
        return null;
    }

}
