package Enums;

public enum PTISTAT {
    IDLE((short)0),
    BUSY((short)1),
    NOCONN((short)2),
    WAITRECON((short)3);

    PTISTAT( short value){
        this.value = value;
    }

    private short value;

    public short getValue(){
        return this.value;
    }
}
