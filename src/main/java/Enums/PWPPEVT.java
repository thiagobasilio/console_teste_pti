package Enums;

public enum  PWPPEVT {

    MAGSTRIPE(1),
    ICC(2),
    CTLS(3),
    ICCOUT(4),
    KEYCONF(17),
    KEYBACKSP(18),
    KEYCANC(19),
    KEYF1(33),
    KEYF2(34),
    KEYF3(35),
    KEYF4(36);


    private  int valor;

    PWPPEVT(int valor){
        this.valor = valor;
    }

    public int getValor() {
        return this.valor;
    }
}
