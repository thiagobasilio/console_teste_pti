package Enums;

public enum SCREEN {
    HOME(1),
    SALE(2);

    private int valor;
    SCREEN(int valor){
        this.valor = valor;
    }

    public int getValor() { return this.valor; }
}
