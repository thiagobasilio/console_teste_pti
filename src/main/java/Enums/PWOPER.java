package Enums;

public enum PWOPER {
    NULL((byte) 0),
    INSTALL((byte)1),
    PARAMUPD((byte)2),
    REPRINT((byte)16),
    RPTTRUNC((byte)17),
    RPTDETAIL((byte)18),
    RPTSUMMARY((byte)21),
    ADMIN((byte)32),
    SALE((byte)33),
    SALEVOID((byte)34),
    PREPAID((byte)35),
    CHECKINQ((byte)36),
    RETBALINQ((byte)37),
    CRDBALINQ((byte)38),
    INITIALIZ((byte)39),
    SETTLEMNT((byte)40),
    PREAUTH((byte)41),
    PREAUTVOID((byte)42),
    CASHWDRWL((byte)43),
    LOCALMAINT((byte)44),
    FINANCINQ((byte)45),
    ADDRVERIF((byte)46),
    SALEPRE((byte)47),
    LOYCREDIT((byte)48),
    LOYCREDVOID((byte)49),
    LOYDEBIT((byte)50),
    LOYDEBVOID((byte)51),
    VOID((byte)57),
    SHOWPDC((byte)251),
    VERSION((byte)252),
    CONFIG((byte)253),
    MAINTENACE((byte)254);

    private byte valor;

    PWOPER(byte valor){
        this.valor = valor;
    }

    public byte getValor(){
        return this.valor;
    }
}
