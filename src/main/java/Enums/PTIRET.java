package Enums;

public enum PTIRET {
    OK((short)0),
    INVPARAM((short)-2001),
    NOCONN((short)-2002),
    BUSY((short)-2003),
    TIMEOUT((short)-204),
    CANCEL((short)-2005),
    NODATA((short)-2006),
    BUFOVRFLW((short)-2007),
    SOCKETERR((short)-2008),
    WRITEERR((short)-2009),
    EFTERR((short)-2010),
    INTERNALERR((short)-2011),
    PROTOCOLERR((short)-2012),
    SECURITYERR((short)-2013),
    PRINTERR((short)-2014),
    NOPAPER((short)-2015),
    NEWCONN((short)-2016),
    NONEWCONN((short)-2017),
    NOTSUPPORTED((short)-2018),
    CRYPTERR((short)-2019);


    private short valor;

    PTIRET(short valor){
        this.valor = valor;
    }

    PTIRET(){
        this.valor = -1;
    }


    public short getValor(){
        return this.valor;
    }

    @Override
    public String toString(){
        return "" + super.toString();
    }
}
