import Enums.SCREEN;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {

    public static final String COMPANY  = "PayGo Test PTI";
    public static final String VERSION  = "1.0";
    public static final String CAPABILITIES  = "63";

    private static Stage window;
    private static Scene sceneHome;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.window = primaryStage;
        this.window.setTitle("EXAMPLE");
        Parent root =  FXMLLoader.load(getClass().getResource("Home.fxml"));
        sceneHome = new Scene(root);
        window.setScene(sceneHome);
        window.show();
    }

    public static void changeScreen(Scene scene){
        window.close();
        window.setScene(scene);
        window.show();
    }

    public static void main(String args[]){
        Application.launch(args);
    }

}
