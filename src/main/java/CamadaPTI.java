import Enums.PTIRET;
import Enums.PTISTAT;
import com.sun.jna.ptr.ShortByReference;
import libPTI.Terminal;

public class CamadaPTI {
    private LibIntegrada ptiLib = new LibIntegrada();

    private ShortByReference status = new ShortByReference((short)2);
    private byte[] pos = new byte[18];
    private byte[] model = new byte[18];
    private byte[] macAdress = new byte[18];
    private byte[] serial = new byte[18];


    //Inicializar
    public PTIRET init(String pos_company, String pos_version, String pos_capabilities, String path, short port,
                       short nTerminal, String pszWaitMsg, short uiAutoDiscSec){
        PTIRET init = ptiLib.pTI_Init(pos_company,pos_version,pos_capabilities,path,port, nTerminal,pszWaitMsg,
                uiAutoDiscSec);
        return init;
    }

    //Checar terminal
    public Terminal checkTerminal(String pos) {
        limparReferencias();
        Terminal terminal = null;
        this.pos = pos.getBytes();
        PTIRET checkStatus = ptiLib.pTI_CheckStatus(this.pos, status, model, macAdress, serial);

        if (checkStatus == PTIRET.OK && !camposVazios()) {
            pos = new String(this.pos);
            terminal = new Terminal(pos);
            terminal.setStatus(convertPTISTAT(status.getValue()));
            terminal.setModel(new String(model));
            terminal.setMacAdress(new String(macAdress));
            terminal.setSerial(new String(serial));
        }
        return terminal;
    }

    public Terminal connectionLoop(){
        limparReferencias();
        Terminal terminal = null;

        PTIRET connectionLoop = ptiLib.pTI_ConnectionLoop(pos,model,macAdress,serial);

        if (PTIRET.NEWCONN == connectionLoop) {
            terminal = new Terminal(new String(pos));
            terminal.setStatus(convertPTISTAT(status.getValue()));
            terminal.setModel(new String(model));
            terminal.setMacAdress(new String(macAdress));
            terminal.setSerial(new String(serial));
        }
        return terminal;
    }

    public boolean disconnect(String pos, short delay){
        if(PTIRET.OK == ptiLib.pTI_Disconnect(pos,delay))
            return true;

        return false;
    }

    public boolean display(String pos, String message){
        if(PTIRET.OK == ptiLib.pTI_Display(pos, message))
            return true;

        return false;
    }

    private void limparReferencias(){
        status = new ShortByReference((short)2);
        pos = new byte[21];
        model = new byte[21];
        macAdress = new byte[18];
        serial = new byte[26];
    }

    private boolean camposVazios(){
        if(model[0] == '\0' && macAdress[0] == '\0' && serial[0] == '\0')
            return true;
        return false;
    }

    private PTISTAT convertPTISTAT(short value){
        for (PTISTAT status: PTISTAT.values()) {
            if(status.getValue() == value)
                return  status;
        }

        return null;
    }

}