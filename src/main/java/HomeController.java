
import Enums.PTIRET;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import java.io.IOException;
import java.net.UnknownHostException;

public class HomeController {

    private CamadaPTI camadaPTI = new CamadaPTI();
    @FXML SaleController saleController = new SaleController();

    @FXML TextField fieldPort;
    @FXML TextField fieldWaitMsg;
    @FXML ComboBox cbAutoDiscSec;
    @FXML ComboBox cbTerminals;
    @FXML TextField fieldTerm1;
    @FXML TextField fieldTerm2;
    @FXML TextField fieldTerm3;
    @FXML Button btnInit;

    @FXML void clickBtnInit(ActionEvent actionEvent) throws UnknownHostException, InterruptedException {
        refreshField();
        if(validaCampos()){
            String pos_company = App.COMPANY;
            String pos_version = App.VERSION;
            String pos_capabilities = App.CAPABILITIES;
            short port = Short.parseShort(fieldPort.getText());
            String pszWaitMsg = fieldWaitMsg.getText();
            short nTerminal = getValueTerminals();
            short uiAutoDiscSec = getValueAutoDiscSec();
            PTIRET init = camadaPTI.init(pos_company,pos_version,pos_capabilities,"Logs",port,
                    nTerminal,pszWaitMsg,uiAutoDiscSec);
            if(init == PTIRET.OK){
                montarTelaTerminais();
            }
        }
    }

    public void clickCbTerminals(ActionEvent actionEvent) {
        disableTerminals();
        int option = cbTerminals.getSelectionModel().getSelectedIndex();
        switch (option){
            case 0 : fieldTerm1.setVisible(true);
                break;
            case 1 : fieldTerm1.setVisible(true);
                     fieldTerm2.setVisible(true);
                break;
            case 2 : fieldTerm1.setVisible(true);
                     fieldTerm2.setVisible(true);
                     fieldTerm3.setVisible(true);
                     break;
        }
    }

    //Obter valor do tempo de ociosidade
    private short getValueAutoDiscSec(){
        switch (cbAutoDiscSec.getSelectionModel().getSelectedIndex()){
            case 0 : return 0;
            case 1 : return 30;
            case 2 : return 60;
            default : return 0;
        }
    }

    //Obter o numero de terminais
    private short getValueTerminals(){
        return (short)(cbTerminals.getSelectionModel().getSelectedIndex()+1);
    }

    //validar se contem apenas numeros em uma string
    private boolean isNumber(String value){
        for ( char c : value.toCharArray()) {
            if(!Character.isDigit(c)){
                return false;
            }
        }
        return true;
    }

    //validar se os campos correspondem aos valores esperados
    private boolean validaCampos(){
        boolean valid = true;
        if (fieldPort.getText().isEmpty() || !isNumber(fieldPort.getText())){
            fieldPort.setStyle("-fx-border-color: red;");
            valid = false;
        }
        if(fieldWaitMsg.getText().isEmpty()){
            fieldWaitMsg.setStyle("-fx-border-color: red;");
            valid = false;
        }
        if (cbAutoDiscSec.getSelectionModel().isEmpty()){
            cbAutoDiscSec.setStyle("-fx-border-color: red;");
            valid = false;
        }
        if (cbTerminals.getSelectionModel().isEmpty()){
            cbTerminals.setStyle("-fx-border-color: red");
            valid = false;
        }

        if ( fieldTerm1.isVisible() && fieldTerm1.getText().isEmpty()){
            fieldTerm1.setStyle("-fx-border-color: red");
            valid = false;
        }

        if ( fieldTerm2.isVisible() && fieldTerm2.getText().isEmpty()){
            fieldTerm2.setStyle("-fx-border-color: red");
            valid = false;
        }

        if ( fieldTerm3.isVisible() && fieldTerm3.getText().isEmpty()){
            fieldTerm3.setStyle("-fx-border-color: red");
            valid = false;
        }
        return valid;
    }

    //tornar as bordas dos campos cinza
    private void refreshField(){
        fieldPort.setStyle("-fx-border-color: grey;");
        fieldWaitMsg.setStyle("-fx-border-color: grey;");
        cbAutoDiscSec.setStyle("-fx-border-color: grey;");
        cbTerminals.setStyle("-fx-border-color: grey;");
    }

    //Ocultar os capos de terminais(PdC)
    private void disableTerminals(){
        fieldTerm1.setVisible(false);
        fieldTerm2.setVisible(false);
        fieldTerm3.setVisible(false);
    }

    private void montarTelaTerminais(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Sale.fxml"));
        try {
            Parent root = (Parent) loader.load();
            int terminais = cbTerminals.getSelectionModel().getSelectedIndex();
            saleController = loader.getController();
            String pos1 = fieldTerm1.getText();
            String pos2 = fieldTerm2.getText();
            String pos3 = fieldTerm3.getText();
            saleController.liberarTerminais(terminais, pos1, pos2, pos3);
            App.changeScreen(new Scene(root));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
